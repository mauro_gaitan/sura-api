<?php
/*
Plugin Name: Plugin Sura API Code
Description: Site with the api code Sura Empresas Code
*/

/**
 * WP API ENDPOINTS & CONFIGURATIONS
 * Prefix url 1: /wp-json/wp/v2
 * Prefix Url 2: ?rest_route=/
 * http://v2.wp-api.org/reference/types/
 * https://1fix.io/blog/2015/07/20/query-vars-wp-api/
 */

define( 'MYPLUG_DIR', plugin_dir_path( __FILE__ ) );

function get_herramientas($request)
{

    $parameters = $request->get_query_params();
    $tematica = !empty($parameters['tematica']) ? $parameters['tematica'] : null;
    $tipo = !empty($parameters['tipo']) ? $parameters['tipo'] : null;
    $word = !empty($parameters['palabra']) ? $parameters['palabra'] : null;

    if ($tipo)
    {
        $args = array(
            'post_type' => 'herramientas',
            //tipo_contenido filter
            'tax_query' => array(
                array(
                    'taxonomy' => 'tipo_contenido',
                    'field' => 'slug',
                    'terms' => $tipo,
                )
            ) ,
            //tag filter
            'tag' => $tematica,
            's' => $word,
            'posts_per_page' => 50,
        );
    }
    else
    {
        $args = array(
            'post_type' => 'herramientas',
            //tag filter
            'tag' => $tematica,
            's' => $word,
            'posts_per_page' => 50,
        );
    }

    $posts = get_posts($args);

    ob_start();

?>
    <?php
    $delayAnimation = 0.2;
    foreach ($posts as $herramienta): ?>
        <?php $docs = get_field('herramientas_descargables', $herramienta->ID); ?>
        <div class="doc-block" style="animation-delay: <?php echo $delayAnimation ?>s;">
            <div class="content">
                <div class="img-group">
                    <?php if ($docs['tipo'] == 'Infografia')
        { ?>
                        <img class="icon-top"
                             src="<?php echo get_template_directory_uri(); ?>/dist/images/icons/Grupo-547.svg">
                    <?php
        }
        else if ($docs['tipo'] == 'Video')
        { ?>
                        <img class="icon-top"
                             src="<?php echo get_template_directory_uri(); ?>/dist/images/icons/Grupo-549.svg">
                    <?php
        }
        else if ($docs['tipo'] == 'PDF')
        { ?>
                        <img class="icon-top"
                             src="<?php echo get_template_directory_uri(); ?>/dist/images/icons/Grupo-330.svg">
                    <?php
        } ?>
                    <img src="<?php echo $docs['imagen']; ?>">
                    <?php if ($docs['clientes'] == 1)
        { ?>
                        <div class="tag">
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/icons/Grupo-541.svg">
                            Exclusivo Clientes
                        </div>
                    <?php
        } ?>
                </div>
                <h4 class="title"><?php echo $herramienta->post_title; ?></h4>
                <p class="description"><?php echo $docs['descripcion']; ?></p>
            </div>
            <div class="cta_doc">
                <?php if ($docs['tipo'] == 'Video')
        { ?>
                    <button class="ver-mas playVideo">VER MAS</button>
                    <button class="ver-mas mobile playVideo" data-container="body" data-toggle="popover" data-placement="bottom"
              data-content="Este servicio es exclusivo para productores y clientes de Solución Integral PyME de SURA.">VER
              MAS</button>
                <?php
        }
        else
        { ?>
                    <?php if ($docs['tipo'] == 'Infografia')
            { ?>
                        <a href="<?php echo $docs['archivo_infografia']; ?>" target="_blank">
                            <button class="download">DESCARGAR</button>
                        </a>
                        <button class="down mobile" data-container="body" data-toggle="popover" data-placement="bottom"
              data-content="Este servicio es exclusivo para productores y clientes de Solución Integral PyME de SURA.">DESCARGAR</button>
                    <?php
            }
            else
            { ?>
                        <a href="<?php echo $docs['archivo_pdf']; ?>" target="_blank">
                            <button class="download">DESCARGAR</button>
                        </a>
                        <button class="down mobile" data-container="body" data-toggle="popover" data-placement="bottom"
              data-content="Este servicio es exclusivo para productores y clientes de Solución Integral PyME de SURA.">DESCARGAR</button>
                    <?php
            } ?>
                <?php
        } ?>
            </div>
            <?php if ($docs['tipo'] == 'Video')
        { ?>
                <div class="video-iframe" style="display: none">
                    <?php echo $docs['url_video'] ?>
                </div>
            <?php
        } ?>
        </div>
        <?php
        $delayAnimation = $delayAnimation + 0.2;
    endforeach; ?>
    <?php
    $contenido = trim(ob_get_clean());

    $return = new stdClass;
    $return
        ->data->status = 'todo bien papu';
    $return
        ->data->codigo = 'alto 200';
    $return
        ->data->contenido = $contenido;

    return new WP_REST_Response($return, 200);
}

add_action('rest_api_init', function ()
{
    register_rest_route("/webapi/", 'herramientas', array(
        'methods' => 'GET',
        'callback' => 'get_herramientas',
        'permission_callback' => function (WP_REST_Request $request)
        {
            return true;
        }
        ,
    ));
});

function set_compare_operator_query($date)
{
    if (empty($date))
    {
        return null;
    }
    return ($date == 'past-events') ? '<=' : '>=';

}

function set_args_meta_query($tematica, $cat, $date)
{

    $args = [];
    //Solo se ejecuta esta funcion si trae algun valor en alguno de los 3 parametros.(click en los filtros)
    if(!$tematica && !$cat && !$date){
        return $args;
    }
    if ($tematica && $cat && !$date)
    {
        $args['tax_query'] = array(
            //Tematica Filter
            array(
                'taxonomy' => 'tematica',
                'field' => 'slug',
                'terms' => $tematica,
            ) ,
            ////Cat Filter
            array(
                'taxonomy' => 'category',
                'field' => 'slug',
                'terms' => $cat,
            )
        );

    }
    if ($tematica && !$cat && !$date)
    {
        $args['tax_query'] = array(
            //Tematica Filter
            array(
                'taxonomy' => 'tematica',
                'field' => 'slug',
                'terms' => $tematica,
            ) ,
        );

    }
    if ($cat && !$tematica && !$date)
    {
        $args['tax_query'] = array(
            //Cat Filter
            array(
                'taxonomy' => 'category',
                'field' => 'slug',
                'terms' => $cat,
            )
        );

    }
    if ($date && $cat && !$tematica)
    {
        $args['tax_query'] = array(
            //Cat Filter
            array(
                'taxonomy' => 'category',
                'field' => 'slug',
                'terms' => $cat,
            )
        );
        $args['meta_key'] = 'cursoscontenido_fecha';
        $args['meta_query'] = array(
            array(
                'key' => 'cursoscontenido_fecha',
                'value' => date('Ymd') ,
                'compare' => $date,
            )
        );

    }
    if ($date && $tematica && !$cat)
    {
        //Tematica Filter
        $args['tax_query'] = array(
            array(
                'taxonomy' => 'tematica',
                'field' => 'slug',
                'terms' => $tematica,
            )
        );
        $args['meta_key'] = 'cursoscontenido_fecha';
        $args['meta_query'] = array(
            array(
                'key' => 'cursoscontenido_fecha',
                'value' => date('Ymd') ,
                'compare' => $date,
            )
        );

    }
    if ($date && $tematica && $cat)
    {
        $args['tax_query'] = array(
            array(
                'taxonomy' => 'tematica',
                'field' => 'slug',
                'terms' => $tematica,
            ) ,
            array(
                'taxonomy' => 'category',
                'field' => 'slug',
                'terms' => $cat,
            )
        );
        $args['meta_key'] = 'cursoscontenido_fecha';
        $args['meta_query'] = array(
            array(
                'key' => 'cursoscontenido_fecha',
                'value' => date('Ymd') ,
                'compare' => $date,
            )
        );

    }
    if ($date && !$tematica && !$cat)
    {
        $args['meta_key'] = 'cursoscontenido_fecha';
        $args['meta_query'] = array(
            array(
                'key' => 'cursoscontenido_fecha',
                'value' => date('Ymd') ,
                'compare' => $date,
            )
        );

    }
    return $args;

}

function get_pagination_content($max_num_pages, $current_page)
{

    $big = 999999999;

    if ($max_num_pages == 0 || !is_numeric($max_num_pages))
    {
        return '';
    }
    $margin = ($max_num_pages == 1) ? "margin-large" : "";

    $html = '<div class="pagination ' . $margin . '">';
    if (($max_num_pages > 1))
    {
        for ($i = 0;$i < $max_num_pages;$i++)
        {

            $page = $i + 1;
            $class = ($current_page == $page) ? 'active' : '';

            $html .= '<span href="#" data-paged="' . $page . '"  class="' . $class . '">' . $page . '</span>';

        }
    }

    $html .= '</div>';
    return $html;

}


function get_content_eventos($the_query){
    ob_start();
    if($the_query->have_posts()):
        while ($the_query->have_posts()):
            $the_query->the_post(); 
            include('eventos.php');
        endwhile;
    else: ?>
        <h3>No se encontraron resultados</h3>
    <?php
    endif;
    return trim(ob_get_clean());
}

function get_cursos_eventos($request)
{
    $parameters = $request->get_query_params();
    
    $per_page = !empty($parameters['per_page']) ? $parameters['per_page'] : 20;
    $current_page = !empty($parameters['paged']) ? $parameters['paged'] : 1;

    $tematica = !empty($parameters['tematica']) ? $parameters['tematica'] : null;
    $cat = !empty($parameters['cat']) ? $parameters['cat'] : null;

    $word = !empty($parameters['palabra']) ? $parameters['palabra'] : null;
    
    
    $date =set_compare_operator_query($parameters['date']);
    
    $meta_args = set_args_meta_query($tematica, $cat, $date);
    
    $args = array_merge(
        $meta_args,
        array(
            'post_type'        =>'cursos-y-eventos',
            'orderby'          => 'meta_value',
            'meta_key' => 'cursoscontenido_fecha',
            'order' => 'ASC',
            'posts_per_page' =>$per_page,
            'paged' =>$current_page,
            's' =>$word
        )
    );

    $the_query = new WP_Query($args);
   
    
    $return = new stdClass;
    $return->data->status = 'ok';
    $return->data->codigo = '200';
    $return->data->contenido = get_content_eventos($the_query);
    $max_num_pages = $the_query->max_num_pages;
    $return->data->paginacion = get_pagination_content($max_num_pages, $current_page);
    return new WP_REST_Response($return, 200);
}

add_action('rest_api_init', function ()
{
    register_rest_route("/webapi/", 'cursos-eventos', array(
        'methods' => 'GET',
        'callback' => 'get_cursos_eventos',
        'permission_callback' => function (WP_REST_Request $request)
        {
            return true;
        }
        ,
    ));
});

function getAsociadoByCuit($request)
{
    global $wpdb;
    $cuit = $request->get_param('cuit');
    $poliza = $request->get_param('poliza');
    $matricula = $request->get_param('matricula');
    $date = date('Ymd');

    $status = false;
    $field = '';
    $msg = '';
    if ( ( $poliza == 7872 && !empty($poliza) ) || (!empty($matricula) && $matricula == 7872 ) ) {
        
        if($poliza == 7872 || $matricula == 7872){
            $status = true;
        } else{
            $field = (!empty($poliza))?'poliza':'matricula';
            $msg = 'Código promocional no válido.';
        }


    
    }else{
        if (empty($matricula))
        {
    
            // Valida Cuit
            $cuit = (int)$cuit;
            $qry = prepare_sql("SELECT * FROM sura_customers WHERE nro_doc = {$cuit}");
            $_customers = $wpdb->get_results($qry . " AND num_pol !=''");
         
        
            if ($_customers and isset($_customers[0]))
            {
                // if (true) {
                $_customers = (array)$_customers[0];
                
                // Validamos si la póliza coincide
                if ($_customers['num_pol'] == $poliza)
                {
    
                    if ($_customers['vig_has'] >= $date)
                    {
                        $status = true;
                    }
                    else
                    {
                        $field = 'poliza';
                        $msg = 'Los datos ingresados no son correctos'; // 'POLIZA ingresada no existe o no está vigente en el registro de clientes Pyme de Seguros SURA.';
                        
                    }
    
                }
                else
                {
                    $field = 'poliza';
                    $msg = 'Los datos ingresados no son correctos'; // 'La combinación CUIT-PÓLIZA ingresada no es válida.';
                    
                }
            }
            else
            {
                $field = 'cuit';
                $msg = 'Los datos ingresados no son correctos'; // 'El CUIT ingresado no existe en el registro de clientes Pyme de Seguros SURA.';
                
            }
        }
        else
        {
            // Valida Cuit
            $cuit = (int)$cuit;
            $qry = prepare_sql("SELECT * FROM sura_customers WHERE nro_doc = {$cuit}");
            $_customers = $wpdb->get_results($qry . " AND matricula !=''");
    
            if ($_customers and isset($_customers[0]))
            {
    
                $_customers = (array)$_customers[0];
    
                // $qry = "SELECT * FROM sura_customers WHERE matricula = '{$matricula}' AND vig_has >= {$date} ";
                // $_customers = $wpdb->get_results( $qry );
                // if ($_customers and isset($_customers[0])) {
                //     $status = true;
                // } else {
                //     $field = 'matricula';
                //     $msg = 'Los datos ingresados no son correctos';// 'La MATRÍCULA ingresada no existe o no está vigente en el registro de clientes Pyme de Seguros SURA.';
                // }
                
    
                // Validamos si la póliza coincide
                if ($_customers['matricula'] == $matricula)
                {
    
                    $status = true;
    
                    // if ($_customers['vig_has'] >= $date) {
                    //     $status = true;
                    // } else {
                    //     $field = 'matricula';
                    //     $msg = 'Los datos ingresados no son correctos';// 'POLIZA ingresada no existe o no está vigente en el registro de clientes Pyme de Seguros SURA.';
                    // }
                    
                }
                else
                {
                    $field = 'matricula';
                    $msg = 'Los datos ingresados no son correctos'; // 'La combinación CUIT-PÓLIZA ingresada no es válida.';
                    
                }
            }
            else
            {
                $field = 'cuit';
                $msg = 'Los datos ingresados no son correctos'; // 'El CUIT ingresado no existe en el registro de clientes Pyme de Seguros SURA.';
                
            }
    
            // if ($matricula == 7872) {
            //     $status = true;
            // } else {
            //     $field = 'matricula';
            //     $msg = 'Código promocional no válido.';
            // }
            
        }
    }
    

    $return['status'] = $status;
    if (!$status)
    {
        $return['error_field'] = $field;
        $return['error_msg'] = $msg;
    }

    return $return;
}

add_action('rest_api_init', function ()
{
    register_rest_route('/webapi/', 'getAsociadoByCuit', array(
        'methods' => 'POST',
        'callback' => 'getAsociadoByCuit',
        'permission_callback' => function (WP_REST_Request $request)
        {
            return true;
        }
    ));
});

function getAsociadosMailsByCuit($request)
{
    global $wpdb;
    $cuit = $request->get_param('cuit');

    $cuit = (int)$cuit;
    $qry = prepare_sql("SELECT * FROM sura_customers WHERE nro_doc = {$cuit}");
    $_customers = $wpdb->get_results($qry);

    $return = [];
    if ($_customers and isset($_customers[0]))
    {
        $_customers = (array)$_customers[0];

        $mail1 = explode('@', $_customers['mail']);
        $mail2 = explode('@', $_customers['mail_productor']);

        $str = "";
        $mail1_1 = substr($mail1[0], 0, 2) . str_pad($str, strlen($mail1[0]) - 2, "x", STR_PAD_LEFT);;
        $mail2_1 = substr($mail2[0], 0, 2) . str_pad($str, strlen($mail2[0]) - 2, "x", STR_PAD_LEFT);

        $str = "";
        $e_mail1 = explode('.', $mail1[1]);
        $x_mail1 = str_pad($str, strlen($e_mail1[0]) , "x", STR_PAD_LEFT);
        $mail1_2 = str_replace($e_mail1[0], $x_mail1, $mail1[1]);

        $e_mail2 = explode('.', $mail2[1]);
        $x_mail2 = str_pad($str, strlen($e_mail2[0]) , "x", STR_PAD_LEFT);
        $mail2_2 = str_replace($e_mail2[0], $x_mail2, $mail2[1]);

        $return['asociado'] = ($_customers['mail'] == 'NO' ? '' : $mail1_1 . '@' . $mail1_2);
        $return['productor'] = $mail2_1 . '@' . $mail2_2;
    }

    return $return;
}

add_action('rest_api_init', function ()
{
    register_rest_route('/webapi/', 'getAsociadosMailsByCuit', array(
        'methods' => 'POST',
        'callback' => 'getAsociadosMailsByCuit',
        'permission_callback' => function (WP_REST_Request $request)
        {
            return true;
        }
    ));
});

function getAsociadoData($request)
{
    global $wpdb;
    $cuit = $request->get_param('cuit');

    $cuit = (int)$cuit;
    $qry = prepare_sql("SELECT * FROM sura_customers WHERE nro_doc = {$cuit}");
    $_customers = $wpdb->get_results($qry);

    $return = [];
    if ($_customers and isset($_customers[0]))
    {
        $return = (array)$_customers[0];
    }

    return $return;
}

add_action('rest_api_init', function ()
{
    register_rest_route('/webapi/', 'getAsociadoData', array(
        'methods' => 'POST',
        'callback' => 'getAsociadoData',
        'permission_callback' => function (WP_REST_Request $request)
        {
            return true;
        }
    ));
});

// Recibe id o correo de usaurio  $email_user_wp
function saveAsociadoAmelia($id_user_wp)
{
    return true;
}

function sendAsociadoMailsPoliza($request)
{
    global $wpdb;
    $cuit = $request->get_param('cuit');
    $razon_social = $request->get_param('empresa');
    $nombre = $request->get_param('nombre');
    $cuit = (int)$cuit;
    $qry = prepare_sql("SELECT * FROM sura_customers WHERE nro_doc = {$cuit}");
    $_customers = $wpdb->get_results($qry);

    $return = [];
    if ($_customers and isset($_customers[0]))
    {
        $_customers = (array)$_customers[0];
        $return['asociado'] = $_customers['mail'];
        $return['productor'] = $_customers['mail_productor'];
        $to = $return['asociado'];
        $subject = 'Seguros Sura :: Nro de Póliza';
        $body = '<h2>Recuperación de póliza</h2><br>
                <p>
                    Estimado cliente, recibidmos una solicitud para obtener el número de póliza. Los datos asociados en nuestra base de datos son: <br>
                &nbsp;&nbsp;&nbsp;<strong>CUIT:</strong>' . $cuit . '<br>
                &nbsp;&nbsp;&nbsp;<strong>Número de póliza:</strong>' . $_customers['num_pol'] . '<br>
                    Ante cualquier duda, no dudes en consultarnos. <br><br>
                    Muchas gracias!<br>
                    Empresas Sura
                </p>';
        $headers = array(
            'Content-Type: text/html; charset=UTF-8'
        );
        mailSendAsociadoMailsPolizaCliente($_customers['mail'], $cuit, $_customers['num_pol']);
        mailSendAsociadoMailsPolizaProductor($_customers['mail_productor'], $cuit, $_customers['num_pol'], $razon_social, $nombre);
        // wp_mail($_customers['mail'], $subject, $body, $headers );
        // wp_mail($_customers['mail_productor'], $subject, $body, $headers );
        return true;
    }
    else
    {
        return false;
    }
}
add_action('rest_api_init', function ()
{
    register_rest_route('/webapi/', 'sendAsociadoMailsPoliza', array(
        'methods' => 'POST',
        'callback' => 'sendAsociadoMailsPoliza',
        'permission_callback' => function (WP_REST_Request $request)
        {
            return true;
        }
    ));
});
function sendAsociadoContacto($request)
{
    global $wpdb;
    $nombre = $request->get_param('nombre');
    $cuit = $request->get_param('cuit');
    $razon = $request->get_param('razon');
    $mail = $request->get_param('mail');
    $telefono = $request->get_param('telefono');
    $cargo = $request->get_param('cargo');

    if (trim($nombre) && $cuit && $razon && $mail && $telefono)
    {
        // $subject = 'Seguros Sura :: Contacto por registro';
        // $body = '<h2>Estimados,</h2><br>
        //         <p>
        //             A continuación, se detalla la consulta realizada desde la web de Empresas Sura:  <br>
        //             Datos de la persona;<br>
        //             &nbsp;&nbsp;&nbsp;<strong>Datos de la persona;
        //             &nbsp;&nbsp;&nbsp;<strong>Nombre y Apellido: :</strong>' . $nombre .  '<br>
        //             &nbsp;&nbsp;&nbsp;<strong>CUIT Empresa: :</strong>' . $cuit .  '<br>
        //             &nbsp;&nbsp;&nbsp;<strong>Razón Social: :</strong>' . $razon .  '<br>
        //             &nbsp;&nbsp;&nbsp;<strong>Mail: :</strong>' . $mail .  '<br>
        //             nbsp;&nbsp;&nbsp;<strong>Telefono: :</strong>' . $telefono .  '<br><br>
        //             Muchas gracias!<br>
        //             Empresas Sura
        //         </p>';
        // $headers = array('Content-Type: text/html; charset=UTF-8');
        $email = 'empresassura@segurossura.com.ar';
        mailSendAsociadoMailsContacto($email, $mail, $cuit, $razon, $nombre, $telefono, $cargo);
        // wp_mail('lucasburella@gmail.com', $subject, $body, $headers );
        return true;
    }

    return false;
}
add_action('rest_api_init', function ()
{
    register_rest_route('/webapi/', 'sendAsociadoContacto', array(
        'methods' => 'POST',
        'callback' => 'sendAsociadoContacto',
        'permission_callback' => function (WP_REST_Request $request)
        {
            return true;
        }
    ));
});

function sendContactForm($request)
{
    global $wpdb;
    $cuit = $request->get_param('cuit');
    $cuit = (int)$cuit;
    $qry = prepare_sql("SELECT * FROM sura_customers WHERE nro_doc = {$cuit}");
    $_customers = $wpdb->get_results($qry);

    $return = [];
    if ($_customers and isset($_customers[0]))
    {
        $_customers = (array)$_customers[0];
        $return['asociado'] = $_customers['mail'];
        $return['productor'] = $_customers['mail_productor'];
        $to = $return['asociado'];
        $subject = 'Seguros Sura :: Nro de Póliza';
        $body = '<h2>Recuperación de póliza</h2><br>
                <p>
                    Estimado cliente, recibidmos una solicitud para obtener el número de póliza. Los datos asociados en nuestra base de datos son: <br>
                &nbsp;&nbsp;&nbsp;<strong>CUIT:</strong>' . $cuit . '<br>
                &nbsp;&nbsp;&nbsp;<strong>Número de póliza:</strong>' . $_customers['num_pol'] . '<br>
                    Ante cualquier duda, no dudes en consultarnos. <br><br>
                    Muchas gracias!<br>
                    Empresas Sura
                </p>';
        $headers = array(
            'Content-Type: text/html; charset=UTF-8'
        );
        mailSendAsociadoMailsPolizaCliente($_customers['mail'], $cuit, $_customers['num_pol']);
        mailSendAsociadoMailsPolizaProductor($_customers['mail_productor'], $cuit, $_customers['num_pol']);
        // wp_mail($_customers['mail'], $subject, $body, $headers );
        // wp_mail($_customers['mail_productor'], $subject, $body, $headers );
        return true;
    }
    else
    {
        return false;
    }
}
add_action('rest_api_init', function ()
{
    register_rest_route('/webapi/', 'sendContactForm', array(
        'methods' => 'POST',
        'callback' => 'sendContactForm',
        'permission_callback' => function (WP_REST_Request $request)
        {
            return true;
        }
    ));
});

function prepare_sql($sql)
{
    global $wpdb;
    $qry = $wpdb->prepare(esc_sql($sql));
    return $qry;
}

