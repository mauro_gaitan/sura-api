<?php
/**
 * WP API ENDPOINTS & CONFIGURATIONS
 * Prefix url 1: /wp-json/wp/v2
 * Prefix Url 2: ?rest_route=/
 * http://v2.wp-api.org/reference/types/
 * https://1fix.io/blog/2015/07/20/query-vars-wp-api/
 */

function get_materiales_lectura($request)
{
    $parameters = $request->get_query_params();
//    $cantidadMaterial = !empty($parameters['cantidadMaterial']) ? $parameters['cantidadMaterial'] : null;
//    $especialidad = !empty($parameters['especialidad']) ? $parameters['especialidad'] : null;

//    $args_materiales = array(
//        'post_type' => 'material',
//        'post_status' => 'publish',
//        'posts_per_page' => $cantidadMaterial,
//    );
//
//    if($especialidad !== ''){
//        $args_materiales['meta_key'] = '_material_especialidad';
//        $args_materiales['meta_value'] = $especialidad;
//    }
//
//
//    $materiales = new WP_Query($args_materiales);
    $return = new stdClass;
//    $aux = [];
//
//    while ($materiales->have_posts()) : $materiales->the_post();
//        array_push($aux, get_post());
//        ?>
    <!--    --><?php
//    endwhile;
//
//    ob_start();
//    foreach ($aux as $post) : ?>
    <!--        <div class="material-content-block">-->
    <!---->
    <!--            <div class="material-content-block-parent">-->
    <!--                <a class="redirect" href="--><?php //echo the_permalink() ?><!--"></a>-->
    <!--                <div class="material-content-block-top">-->
    <!--                    <span class="especialidad">--><?//= get_post_field('_material_especialidad') ?><!--</span>-->
    <!--                    <span class="date"> --><?//= get_the_date('d / m / Y') ?><!--</span>-->
    <!--                </div>-->
    <!---->
    <!--                <div class="material-content-block-mid">-->
    <!--                    <span class="titulo-material-block"> <a href="--><?php //echo the_permalink() ?><!--" target="_blank">--><?//= $post->post_title ?><!--</a></span>-->
    <!--                    <span-->
    <!--                        class="subtitulo-material-block"> --><?//= substr(strip_tags(get_post_field('_material_contenido')), 0, 40) . '...'; ?>
    <!--              </span>-->
    <!--                </div>-->
    <!---->
    <!--                <div class="material-content-block-bottom">-->
    <!---->
    <!--                    <a href="--><?php //echo the_permalink() ?><!--" class="rubik-medium" >leer más</a>-->
    <!---->
    <!--                </div>-->
    <!---->
    <!--            </div>-->
    <!---->
    <!--        </div>-->
    <!--    --><?php
//    endforeach;
//
//    $convertido = trim(ob_get_clean());
    $return->data->status = 'todo bien papu';
    $return->data->codigo = 'alto 200';

    return new WP_REST_Response($return, 200);
}

add_action('rest_api_init', function () {
    register_rest_route("/webapi/", 'materiales-de-lectura', array(
        'methods' => 'GET',
        'callback' => 'get_materiales_lectura',
        'permission_callback' => function (WP_REST_Request $request) {
            return true;
        },
    ));
});

