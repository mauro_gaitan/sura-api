<?php 
class Paginator {
  
  private $_conn;
     private $_limit;
     private $_page;
     private $_total;

     public function __construct( $total ,$page,$limit) {
      
 
        $this->_total = $total;
        $this->_page = $page;
        $this->_limit = $limit;

          
    }



    public function createLinks( $links ) {
        if ( $this->_limit == 'all' ) {
            return '';
        }
      
        $last       = ceil( $this->_total / $this->_limit );
      
        $start      = ( ( $this->_page - $links ) > 0 ) ? $this->_page - $links : 1;
        $end        = ( ( $this->_page + $links ) < $last ) ? $this->_page + $links : $last;
      
        $html       = '<ul class="pagination">';
      
        $class      = ( $this->_page == 1 ) ? "disabled" : "";
        $html       .= '<li class="' . $class . '"><span href="#" data-paged="'.$this->_page.'">&laquo;</span ></li>';
      
        if ( $start > 1 ) {
            $html   .= '<li><span href="#" data-perpage="'.$this->_limit .'" data-paged="'.$this->_page.'">1</span ></li>';
            $html   .= '<li class="disabled"><span>...</span></li>';
        }
      
        for ( $i = $start ; $i <= $end; $i++ ) {
            $class  = ( $this->_page == $i ) ? "active" : "";
            $html   .= '<li class="' . $class . '"><span href="#" data-perpage="'.$this->_limit .'" data-paged="'.$this->_page.'">' . $i . '</span ></li>';
        }
      
        if ( $end < $last ) {
            $html   .= '<li class="disabled"><span>...</span></li>';
            $html   .= '<li><span href="#" data-perpage="'.$this->_limit .'" data-paged="'.$this->_page.'">' . $last . '</span ></li>';
        }
      
        $class      = ( $this->_page == $last ) ? "disabled" : "";
        $html       .= '<li class="' . $class . '"><span href="#" data-perpage="'.$this->_limit .'" data-paged="'.$this->_page.'">&raquo;</span ></li>';
      
        $html       .= '</ul>';
      
        return $html;
    }

}