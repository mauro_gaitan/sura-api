<?php 



      
         
            
        $fields = get_field('cursoscontenido');
        $fecha = $fields['fecha'];
    
    
       $hora = $fields['hora'];
       $titulo = $fields['titulo'];
       $modalidad = $fields['modalidad'];
       $facilitador = $fields['facilitador'];
       $id_amelia = $fields['id_amelia'];
       $exclusivo = $fields['exclusivo_clientes'];
   
    ?>
    <div class="block">
                        <div class="date-time">
                            <p>Inicio</p>
                            <h5 class="date">
                                  <?php echo date_i18n('d', strtotime($fecha)); ?>
                                  <?php echo date_i18n('M', strtotime($fecha)); ?>
                                  <?php echo date_i18n('Y', strtotime($fecha)); ?>
                                </h5>
                            <p><?php echo $hora; ?> hs.</p>
                        </div>
                        <div class="event-data">
                            <div class="right">
                                <p class="tag">
                                    <?php $terms = get_the_category(get_the_ID(), 'board');
                                    foreach ($terms as $term)
                                    {
                                        echo $term->name;
                                    } ?>            
                                </p>
                                <p class="title"><?php the_title(); ?></p>
                                <p class="small">Modalidad: <?php echo $modalidad; ?></p>
                                <p class="small">Facilitador: <?php echo $facilitador; ?></p>
                            </div>
                            <div class="cta">
                                <?php if ($exclusivo == 1){ ?>
                                    <button class="exclusivo">
                                        <img src="<?php echo get_template_directory_uri(); ?>/dist/images/icons/Grupo-541.svg">
                                        Exclusivo Clientes
                                    </button>
                                <?php } ?>
    
                                <a href="<?php echo $post->guid ?>?ameliaid=<?php echo $id_amelia; ?>">
                                    <button class="inscribirse">
                                        <img src="" class="icon">
                                        VER MÁS
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
            
 
       

    
   